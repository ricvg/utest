/*
 * Copyright 2012 Ricardo Villalobos Guevara
 */
//............................................................................
#define UTEST_DEFINE_GLOBALS
#include "utest.h"
//............................................................................
static void UTestData_ctor(struct UTestData* const);
static void UTestData_reset(struct UTestData* const);
static inline int UTestData_getDbg(struct UTestData* const);
static void UTestData_setDbg(struct UTestData* const);
static void UTestData_clrDbg(struct UTestData* const);
static void UTestData_setExitOnError(struct UTestData* const);
static void UTestData_clrExitOnError(struct UTestData* const);
// ===========================================================================
void utest_init(void)
{
	UTestData_ctor(utest_data__);
}
//............................................................................
void utest_enableDebugPrint(void)
{
	UTestData_setDbg(utest_data__);
}
//............................................................................
void utest_disableDebugPrint(void)
{
	UTestData_clrDbg(utest_data__);
}
//............................................................................
int utest_isDebugPrintEnabled(void)
{
	return UTestData_getDbg(utest_data__);
}
//............................................................................
void utest_enableExitOnFailed_TEST_REQUIRE(void)
{
	UTestData_setExitOnError(utest_data__);
}
//............................................................................
void utest_disableExitOnFailed_TEST_REQUIRE(void)
{
	UTestData_clrExitOnError(utest_data__);
}
// ===========================================================================
void on_assert__(const char* file, unsigned line, int error)
{
	utest_data__->file = file;
	utest_data__->line = line;
	utest_data__->error = error;
	longjmp(utest_data__->jmpBuf, 1);
}
//............................................................................
static inline void UTestData_ctor(struct UTestData* const data)
{
	UTestData_reset(data);
}
//............................................................................
static inline void UTestData_reset(struct UTestData* const data)
{
	memset(data, 0, sizeof(data));
}
//............................................................................
static inline int UTestData_getDbg(struct UTestData* const data)
{
	return data->dbg;
}
//............................................................................
static inline void UTestData_setDbg(struct UTestData* const data)
{
	data->dbg = 1;
}
//............................................................................
static inline void UTestData_clrDbg(struct UTestData* const data)
{
	data->dbg = 0;
}
//............................................................................
static inline void UTestData_setExitOnError(struct UTestData* const data)
{
	data->exitOnFailedRequirement = 1;
}
//............................................................................
static inline void UTestData_clrExitOnError(struct UTestData* const data)
{
	data->exitOnFailedRequirement = 0;
}
//............................................................................
