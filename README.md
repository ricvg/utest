# uTest #

Mini-framework to help unit test C code by using C preprocessor macros and setjump() from the C standard library.

See [ricvg-clib](https://bitbucket.org/ricvg/ricvg-clib) for an example on how this is used.