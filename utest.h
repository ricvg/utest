/*
 * Copyright 2012 Ricardo Villalobos Guevara
 */
#ifndef __UTEST_H_
#define __UTEST_H_
//............................................................................
#include <setjmp.h>
//............................................................................
#ifdef __cplusplus
extern "C" {
#endif
#include <assert.h>
#include <setjmp.h>
#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif
//............................................................................
#define EXIT_FAILURE 1
//............................................................................
#if __STDC_VERSION__ < 199901L
# if __GNUC__ < 2
#  define __func__ "<unknown>"
# endif
#else
#  define __func__ "<unknown>"
#endif
//............................................................................
#define __FUNC_UNDER_TEST(func_, expect_assertion__, error_code, testfile_, testline_) \
	if(! setjmp(utest_data__->jmpBuf)){ \
		if(0 != strcmp(utest_data__->lastFnc, __func__)){ \
			strcpy(utest_data__->lastFnc, __func__); \
			if(utest_data__->dbg) printf("TEST %s():\n", __func__); \
		} \
		if(utest_data__->dbg) printf("\t%s ... ", #func_); \
		(func_); \
		if(utest_data__->dbg) printf("OK\n"); \
		if (expect_assertion__) \
			return EXIT_FAILURE; \
	}else{ \
		if(utest_data__->dbg) {\
			printf("ASSERTION\n\t    ");\
			if(utest_data__->error == error_code) { \
				printf("%s failed with expected %s assertion with code 0x%04X.\n", \
				       #func_, #error_code, utest_data__->error); \
			} else { \
				printf("%s failed with error code 0x%04X in line %d at %s. Expected error %s with code 0x%04X. Test file %s in line %d.\n", \
					#func_, utest_data__->error, utest_data__->line, utest_data__->file, #error_code, error_code, testfile_, testline_); \
			} \
		} \
		if ( ! expect_assertion__) \
			return EXIT_FAILURE; \
	}
//............................................................................
#define EXPECT_ASSERTION(func__, error_code)    __FUNC_UNDER_TEST(func__, 1, error_code, __FILE__, __LINE__)
#define EXPECT_NO_ASSERTIONS(func__) __FUNC_UNDER_TEST(func__, 0, 0, __FILE__, __LINE__)
//............................................................................
#define TEST_REQUIRE(test_) \
	if( ! (test_)){ \
		if(utest_data__->dbg) { \
			printf("\t    ERROR: TEST_REQUIRE(%s) failed in function \n\t           %s() in line %d of file %s\n", #test_, __func__, __LINE__, __FILE__); \
		} \
		if(utest_data__->exitOnFailedRequirement) { \
			if(utest_data__->dbg) { \
				printf("Test stopped because of failed test condition.\n"); \
			} \
			exit(EXIT_FAILURE); \
		} \
	}
//............................................................................
struct UTestData {
	const char* file;
	unsigned line;
	int error;
	jmp_buf jmpBuf;
	char lastFnc[500];
	unsigned dbg;
	unsigned exitOnFailedRequirement;
};
//............................................................................
#ifdef UTEST_DEFINE_GLOBALS
struct UTestData  utest_data_instance__;
struct UTestData* utest_data__ = &utest_data_instance__;
#else
extern struct UTestData* utest_data__;
#endif
//............................................................................
void utest_init(void);
void utest_enableDebugPrint(void);
int utest_isDebugPrintEnabled(void);
void utest_enableExitOnFailed_TEST_REQUIRE(void);
void utest_disableExitOnFailed_TEST_REQUIRE(void);
//............................................................................
#ifdef __cplusplus
}
#endif
//............................................................................
#endif /* __UTEST_H_ */
